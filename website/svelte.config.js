import adapter from '@sveltejs/adapter-auto';
import preprocess from "svelte-preprocess";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter(),
    outDir: 'publish',
    csp: {
      directives: {
        'script-src': ['self']
      }
    }
  },
  preprocess: [
    preprocess({
      postcss: true,
    }),
  ],
};

export default config;