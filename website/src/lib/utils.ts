import { browser } from '$app/environment'
import { toast } from '@zerodevx/svelte-toast'
import { get } from 'svelte/store';
import { user } from '../stores/store';
import { createAuthToken } from './security';
import { createAuthClient } from '$lib/auth.ts';
import { PUBLIC_LOCAL_URL } from '$env/static/public';

const makeAPIRequest = async (endpoint: string, body: Object) => {
    var t = await fetch(endpoint, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': createAuthToken()
        },
        body: JSON.stringify(body)
    })

    t = await t.json();
    console.log(t)
    return t
}

export const DB: DBInterface = {
    findOneItem: async (collection: string, query: Object) => {
        var t = await makeAPIRequest('/api/simple_find.json', { collection, regex: query });
        return t.body;
    },
    findMultipleItem: async (collection: string, query: Object) => {
        var t = await makeAPIRequest('/api/find_to_array.json', { collection, regex: query });
        return t.body;
    },
    insertItem: async (collection: string, query: Object) => {
        var t = await makeAPIRequest('/api/simple_write.json', { collection, items: query });
        return t.body;
    },
    editItem: async (collection: string, regex: Object, query: Object) => {
        var t = await makeAPIRequest('/api/simple_edit.json', { collection, regex, items: query });
        return t.body;
    },
    deleteItem: async (collection: string, query: Object) => {
        var t = await makeAPIRequest('/api/simple_delete.json', { collection, regex: query });
        return t.body;
    }
}

export const JWT = {
    sign: async (payload: Object) => {
        var res = await fetch('/api/jwt_sign.json', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': createAuthToken()
            },
            body: JSON.stringify({
                payload: payload
            })
        })

        let actualRes: ServerJWTResponse = await res.json();
        return actualRes.body.jwt || false;
    },
    unsign: async (token: string) => {
        var res = await fetch('/api/jwt_verify.json', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': createAuthToken()
            },
            body: JSON.stringify({
                token: token
            })
        })

        res = await res.json();
        return res.body;
    }
}

export const Quickies = {
    href: (url: string) => {
        if (browser) {
            window.location.href = url;
        }
    },
    toast: {
        success: (message: string) => {
            toast.push(message, {
                classes: ['toast-success']
            });
        },
        error: (message: string) => {
            toast.push(message, {
                classes: ['toast-danger']
            });
        },
        warning: (message: string) => {
            toast.push(message, {
                classes: ['toast-warn']
            });
        }
    },
    logout: async () => {
        let auth0 = await createAuthClient();
        console.log(PUBLIC_LOCAL_URL)
        await auth0.logout();
    }
}

export const UserLoginFlow = async () => {
    var userJWT = get(user)

    if (typeof userJWT == 'string') {
        var unsignedUser: any = await JWT.unsign(userJWT);
        // console.log(unsignedUser)

        if (unsignedUser.message && unsignedUser.message == 'jwt expired') {
            user.set(null)
            redirectToLogin();
        } else if (unsignedUser.id) {
            if (window.location.pathname == "/") window.location.href = "/dashboard"
            return unsignedUser;
        }
    }

    redirectToLogin();

}

const redirectToLogin = () => {
    if (window.location.pathname !== "/") {
        window.location.href = "/"
    }
}

export const validateInput = (e: any, opts: InputValidation) => {
    var input = e.target.value;

    opts = opts || {};

    for(var i in opts) {
        switch(i) {
            case 'lower_case':
                if(!opts[i]) break;
                input = input.toLowerCase();
                break;
            case 'upper_case':
                if(!opts[i]) break;
                input = input.toUpperCase();
                break;
            case 'no_space':
                if(!opts[i]) break;
                input = input.replace(/\s/g, '-');
                break;
            case 'length':
                if(!opts[i]) break;
                if (input.length > (opts[i] || 0)) {
                    input = input.substring(0, opts[i]);
                }
                break;
        }
    }

    e.target.value = input;
}

