interface APIResponse {
    status: boolean,
    msg: string,
    data: any
}

interface ServerJWTResponse {
    body: {
        jwt: string
        message: string
    }
}

interface InputValidation {
    lower_case?: boolean, // makes text all lowercase
    upper_case?: boolean, // makes text all uppercase
    no_space?: boolean, // replaces all spaces with hyphens
    length?: number, // max length of text
}

interface DBInterface {
    findOneItem: Function,
    findMultipleItem: Function,
    insertItem: Function,
    editItem: Function,
    deleteItem: Function,
}