import { verifyAuthToken } from './lib/security';

/** @type {import('@sveltejs/kit').Handle} */
export async function handle({ event, resolve, fetch }) {
  if(event?.request?.url.includes('/api/')) {
    if(event.request.headers.get('authorization')) {
        let token = event.request.headers.get('authorization');
        if(verifyAuthToken(token)) {
          return resolve(event);
        } else {
          return new Response("Unauthorized", {
            status: 401,
            statusText: "Unauthorized"
          });
        }
    } else {
        return new Response("Unauthorized", {
          status: 401,
          statusText: "Unauthorized"
        });
    }
  }else{
    return resolve(event);
  }
}