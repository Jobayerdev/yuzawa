import jwt, { type Secret } from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

export const POST = async ({ params, request }: { params: Object, request: Request }) => {
    var b = await request.json()
    var jwt_secret: Secret = process.env.JWT_SECRET || "";

    let t: string = jwt.sign(b.payload, jwt_secret, {
        expiresIn: '12h',
    });

    return new Response(JSON.stringify({
        status: 200,
        body: {
            jwt: t
        }
    }))
}