import { connectToDatabase } from '$lib/db.js';
import { ObjectId } from 'mongodb'

export const POST = async ({ params, request }: { params: Object, request: Request }) => {
    var b = await request.json()
    // console.log(b)
    let dbTest = await connectToDatabase();
    // console.log(dbTest.db)

    if (b.regex._id && !b.hold) b.regex._id = new ObjectId(b.regex._id)

    var c = await dbTest.db.collection(b.collection).find(b.regex).toArray()

    let response: APIResponse = {
        status: c ? true : false,
        msg: c ? "Database returned something!" : "Database had an error/returned nothing.",
        data: c
    }

    return new Response(JSON.stringify({
        status: 200,
        body: response
    }))
}