import { browser } from '$app/environment';
import { UserLoginFlow } from '$lib/utils.ts';

export const ssr = false;

export async function load() {
    if (!browser) {
        return {};
    }

    let userToken = await UserLoginFlow();

    return {
        status: 200,
        userToken
    };
}