import { browser } from '$app/environment';
import {
    DB,
    UserLoginFlow,
} from '/src/lib/utils';

export const ssr = false;

export async function load() {
    if (browser) {
        const t = await UserLoginFlow();

        let modelZoos = await DB.findMultipleItem('model-zoo', { status: "active" });

        return {
            status: 200,
            modelZoos: modelZoos.data,
            userInformation: {
                id: t.id
            }
        }
    }
}