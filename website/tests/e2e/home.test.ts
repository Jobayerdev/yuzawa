import { expect, test } from '@playwright/test';

test('Home ', async ({ page }) => {
	await page.goto('/');
	expect(await page.textContent('h1')).toBe('Welcome to Yuzawa.');
	expect(await page.textContent('p')).toBe(
		'A Computer Architecture Simulation-as-a-Service Platform.'
	);
});
